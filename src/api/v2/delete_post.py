import boto3
from http import HTTPStatus

def get_dynamo_table():
    dynamodb = boto3.resource("dynamodb")
    return dynamodb.Table('PostsTable')

def lambda_handler(event, context):

    table = get_dynamo_table()

    key = event["path"]["id"]
    response = table.get_item(Key={
    "_id": key,
    })

    if "Item" not in response:
        return {
            "statusCode": HTTPStatus.NOT_FOUND.value, #404
            "body": HTTPStatus.NOT_FOUND.phrase
        }

    else:
        response = table.delete_item(Key={
        "_id": key,
        },
        ReturnValues='ALL_OLD')

        data = response["Attributes"]

        return {
                "Old Attributes": data,
                'statusCode': HTTPStatus.OK.value #200
            }
