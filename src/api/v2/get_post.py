from http.client import responses
import boto3
from http import HTTPStatus
 
def get_dynamo_table():
    dynamodb = boto3.resource("dynamodb")
    return dynamodb.Table('PostsTable')

def get_all(event, context):

    table = get_dynamo_table()

    response = table.scan()
    
    data = response["Items"]

    if bool(data) == False: # if there are no items
        return {
            "body": [],
            'statusCode': HTTPStatus.NO_CONTENT.value #204
        }

    # body = json.dumps(data) => I opted for returning the data in the form of a dictionary
    
    return {
        "body": data,
        'statusCode': HTTPStatus.OK.value #200
    }

def get_one(event, context):
    table = get_dynamo_table()
    
    key = event["path"]["id"]
        
    response = table.get_item(Key={
    "_id": key,
    })

    if "Item" not in response:
        return {
            "statusCode": HTTPStatus.NOT_FOUND.value, #404
            "body": HTTPStatus.NOT_FOUND.phrase
        }

    else:
        data = response["Item"]
        return {
            "body": data,
            'statusCode': HTTPStatus.OK.value #200
        }


