import boto3
from http import HTTPStatus

def get_dynamo_table(): # for the sake of organization
    dynamodb = boto3.resource("dynamodb")
    return dynamodb.Table('PostsTable')

def lambda_handler(event, context):

    body = event["body"]

    table = get_dynamo_table()

    key = event["path"]["id"]
    response = table.get_item(Key={
    "_id": key,
    })

    if "Item" not in response:
        return {
            "statusCode": HTTPStatus.NOT_FOUND.value, #404
            "body": HTTPStatus.NOT_FOUND.phrase
        }

    # dealing with one or more elements not being in the body
    elif not("author" in body and "title" in body and "body" in body and "medias" in body):
        return {
            "statusCode": HTTPStatus.UNPROCESSABLE_ENTITY.value, #422
            "body": "No author, title, body or media provided"
        }
    
    # dealing with one or more elements not being of the right type
    is_author_type = True if (type(body["author"]) == str) else False
    is_title_type = True if (type(body["title"]) == str) else False
    is_body_type = True if (type(body["body"]) == str) else False
    is_medias_type = True if (type(body["medias"]) == list) else False

    if not(is_author_type and is_title_type and is_body_type and is_medias_type):
        return {
            "statusCode": HTTPStatus.UNSUPPORTED_MEDIA_TYPE.value, #415
                "body": HTTPStatus.UNSUPPORTED_MEDIA_TYPE.phrase #"Unsupported Media Type"
        }

    #successful update
    else:
        response = table.put_item(
            Item={
                "_id": key,
                'author': body["author"],
                'title': body["title"],
                "body": body["body"],
                "medias": body["medias"]
            },
            ReturnValues='ALL_OLD'
        )


        data = response["Attributes"]

        return {
                "body": data,
                'statusCode': HTTPStatus.OK.value #200
            }
