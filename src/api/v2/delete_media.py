import boto3
from http import HTTPStatus

def get_dynamo_table():
    dynamodb = boto3.resource("dynamodb")
    return dynamodb.Table('PostsTable')

def lambda_handler(event, context):
    body = event["body"]

    #checking if all elements are in body
    if ("id" in body and "bucket" in body and "key" in body and "region" in body):
        #checking if the elements are of the right type
        is_id_type = True if (type(body["id"]) == str) else False
        is_bucket_type = True if (type(body["bucket"]) == str) else False
        is_key_type = True if (type(body["key"]) == str) else False
        is_region_type = True if (type(body["region"]) == str) else False

        if not(is_id_type and is_bucket_type and is_key_type and is_region_type):
            return {
                "statusCode": 415,
                "body": "Unsupported Media Type"
            }

        key = body["key"] #the media name
        bucket = body["bucket"]
        region = body["region"]
        if region == "us-east-1":
            url = f'https://{bucket}.s3.amazonaws.com/{key}'
        else:
            url = f'https://{bucket}.s3.{region}.amazonaws.com/{key}'

        table = get_dynamo_table()
    
        #getting item whose information is going to be changed
        response = table.get_item(Key={
        "_id": body["id"],
        })

        old_media = response["Item"]["medias"]
        if url in old_media:
            old_media.remove(url)
        else: 
            return {
                "statusCode": HTTPStatus.NOT_FOUND.value, #404
                "body": HTTPStatus.NOT_FOUND.phrase
            }

        response = table.update_item(
            Key={
                "_id": body["id"]
            },
            UpdateExpression='SET medias = :media',
            ExpressionAttributeValues={
                ':media': old_media
            },
            ReturnValues='ALL_NEW'
        )

        data = response["Attributes"]

        return {
                'statusCode': HTTPStatus.OK.value, #200
                "Updated Attributes": data
            }

    else:
        return {
            "statusCode": HTTPStatus.UNPROCESSABLE_ENTITY.value, #422
            "body": "No id, bucket, key or region provided"
        }