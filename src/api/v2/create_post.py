import boto3
import uuid
from http import HTTPStatus
 
def get_dynamo_table(): # for the sake of organization
    dynamodb = boto3.resource("dynamodb")
    return dynamodb.Table('PostsTable')


def lambda_handler(event, context): # main function
    body = event["body"]

    #checking if all elements are in body
    if ("author" in body and "title" in body and "body" in body and "medias" in body):
        
        #checking if the elements are of the right type
        is_author_type = True if (type(body["author"]) == str) else False
        is_title_type = True if (type(body["title"]) == str) else False
        is_body_type = True if (type(body["body"]) == str) else False
        is_medias_type = True if (type(body["medias"]) == list) else False

        if not(is_author_type and is_title_type and is_body_type and is_medias_type):
            return {
                "statusCode": HTTPStatus.UNSUPPORTED_MEDIA_TYPE.value, #415
                "body": HTTPStatus.UNSUPPORTED_MEDIA_TYPE.phrase #"Unsupported Media Type"
            }


        table = get_dynamo_table()
 
        table.put_item(
            Item={
                "_id":    str(uuid.uuid4()),
                "author": body["author"],
                "title": body["title"],
                "body": body["body"],
                "medias": body["medias"]
            }
        )
 
        return {
            "statusCode": HTTPStatus.CREATED.value, #201
            "body": HTTPStatus.CREATED.phrase, #"Created"
        }
 
    return {
        "statusCode": HTTPStatus.UNPROCESSABLE_ENTITY.value, #422
        "body": "No author, title, body and/or media provided"
    }